package com.example.listamercado.entities;

public class Produto {
    private Long id;
    private String descricao;
    private double preco;
    private Setor setor;

    public Produto() {

    }

    public Produto(Long id, String descricao, double preco, Setor setor) {
        this.id = id;
        this.descricao = descricao;
        this.preco = preco;
        this.setor = setor;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public Setor getSetor() {
        return setor;
    }

    public void setSetor(Setor setor) {
        this.setor = setor;
    }
}
