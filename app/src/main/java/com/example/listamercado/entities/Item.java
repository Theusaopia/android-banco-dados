package com.example.listamercado.entities;

public class Item {
    private Long id;
    private double quantidade;
    private boolean comprado;
    private ListaCompras lista;
    private Produto produto;

    public Item() {

    }

    public Item(Long id, double quantidade, boolean comprado, ListaCompras lista, Produto produto) {
        this.id = id;
        this.quantidade = quantidade;
        this.comprado = comprado;
        this.lista = lista;
        this.produto = produto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(double quantidade) {
        this.quantidade = quantidade;
    }

    public boolean isComprado() {
        return comprado;
    }

    public void setComprado(boolean comprado) {
        this.comprado = comprado;
    }

    public ListaCompras getLista() {
        return lista;
    }

    public void setLista(ListaCompras lista) {
        this.lista = lista;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }
}
