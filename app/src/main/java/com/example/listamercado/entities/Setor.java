package com.example.listamercado.entities;

import java.util.List;

public class Setor {
    private Long id;
    private String nome;
    private List<Produto> prods;

    public Setor() {

    }

    public Setor(Long id, String nome, List<Produto> prods) {
        this.id = id;
        this.nome = nome;
        this.prods = prods;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Produto> getProds() {
        return prods;
    }

    public void setProds(List<Produto> prods) {
        this.prods = prods;
    }
}
