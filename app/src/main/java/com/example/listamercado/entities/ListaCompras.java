package com.example.listamercado.entities;

import java.util.List;

public class ListaCompras {
    private Long id;
    private String nome;
    private int prioridade;
    private double valorEstimado;
    private List<Item> itens;

    public ListaCompras() {

    }

    public ListaCompras(Long id, String nome, int prioridade, double valorEstimado, List<Item> itens) {
        this.id = id;
        this.nome = nome;
        this.prioridade = prioridade;
        this.valorEstimado = valorEstimado;
        this.itens = itens;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getPrioridade() {
        return prioridade;
    }

    public void setPrioridade(int prioridade) {
        this.prioridade = prioridade;
    }

    public double getValorEstimado() {
        return valorEstimado;
    }

    public void setValorEstimado(double valorEstimado) {
        this.valorEstimado = valorEstimado;
    }

    public List<Item> getItens() {
        return itens;
    }

    public void setItens(List<Item> itens) {
        this.itens = itens;
    }
}
