package com.example.listamercado;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BancoHelper extends SQLiteOpenHelper {

    public BancoHelper(Context ctx) {
        super(ctx, "listinha", null, 2);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table setor  (_id integer primary key, nome varchar(300) )");
        db.execSQL("create table produto(_id integer primary key, nome varchar(300), "+
                " quantidade numeric(18,3), comprado char(1) default 'N'," +
                " id_setor integer not null references setor(_id))");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion > 1) {
            db.execSQL("create table produto(_id integer primary key, nome varchar(300), " +
                    " quantidade numeric(18,3), comprado char(1) default 'N'," +
                    " id_setor integer not null references setor(_id))");
        }
    }
}
