package com.example.listamercado;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cursoradapter.widget.SimpleCursorAdapter;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Produtos extends AppCompatActivity implements AdapterView.OnItemLongClickListener {

    long id_setor;
    SQLiteDatabase banco;
    BancoHelper helper;
    Cursor cursor;

    SimpleCursorAdapter adapter;
    ListView listaProdutos;
    EditText nomeProduto, quantidadeProduto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produtos);

        listaProdutos = (ListView) findViewById(R.id.lst_produtos);

        Intent it = getIntent();
        String nomeSetor = it.getStringExtra("nome_setor");
        ((TextView) findViewById(R.id.txt_setor)).setText(nomeSetor);
        id_setor = it.getLongExtra("id_setor", 0);

        helper = new BancoHelper(getApplicationContext());
        banco = helper.getWritableDatabase();
        cursor = banco.query("produto",
                new String[] {"_id", "nome", "quantidade", "comprado"},
                "id_setor = ?",
                new String[] {String.valueOf(id_setor)},
                null,
                null,
                "nome");

        adapter = new SimpleCursorAdapter( getApplicationContext(), R.layout.item_lista_produtos,
                cursor, new String[] {"nome", "quantidade", "comprado"}, new int[] {R.id.item_nome_produto, R.id.item_quantidade, R.id.item_comprado},
                SimpleCursorAdapter.NO_SELECTION);

        listaProdutos.setAdapter(adapter);
        listaProdutos.setOnItemLongClickListener(this);
    }

    public void confirmar(View view) {
        nomeProduto = (EditText) findViewById(R.id.edProduto);
        quantidadeProduto = (EditText) findViewById(R.id.edQtde);

        String nome = nomeProduto.getText().toString();
        int quantidade = Integer.parseInt(quantidadeProduto.getText().toString());

        if(nome != "" && quantidade > 0) {
            ContentValues cv = new ContentValues();
            cv.put("nome", nome);
            cv.put("quantidade", quantidade);
            cv.put("comprado", "N");
            cv.put("id_setor", id_setor);

            long id = banco.insert("produto", null, cv);

            if(id > 0) {
                atualizarLista();
                nomeProduto.setText("");
                quantidadeProduto.setText("");
            } else {
                Toast.makeText(this, "Falha na inserção", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void atualizarLista() {
        cursor.close();
        cursor = banco.query("produto",
                new String[] {"_id", "nome", "quantidade", "comprado"},
                "id_setor = ?",
                new String[] {String.valueOf(id_setor)},
                null,
                null,
                "nome");
        adapter.changeCursor(cursor);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void finish() {
        banco.close();
        super.finish();
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long id) {
        ContentValues cv = new ContentValues();
        cv.put("comprado", "S");

        int cont = banco.update("produto", cv, "_id = ?", new String[] {String.valueOf(id)});

        if(cont > 0) {
            atualizarLista();
        }else {
            Toast.makeText(this, "Falha na atualizacao", Toast.LENGTH_SHORT).show();
        }
        return true;
    }
}