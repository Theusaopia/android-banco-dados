package com.example.listamercado;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cursoradapter.widget.SimpleCursorAdapter;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemLongClickListener, AdapterView.OnItemClickListener {

    SQLiteDatabase banco;
    BancoHelper helper;

    SimpleCursorAdapter adapter;
    ListView lista;
    Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        helper = new BancoHelper( getApplicationContext() );
        banco = helper.getWritableDatabase();
        cursor = banco.query("setor", new String[] {"_id", "nome"},
                null, null, null, null, "nome");

        adapter = new SimpleCursorAdapter( getApplicationContext(), R.layout.item_lista,
                cursor, new String[] { "_id", "nome"}, new int[] {R.id.item_id, R.id.item_nome},
                SimpleCursorAdapter.NO_SELECTION);

        lista = (ListView) findViewById(R.id.lista);

        lista.setAdapter(adapter);

        lista.setOnItemLongClickListener( this );

        lista.setOnItemClickListener( this );
    }

    @Override
    protected void onDestroy() {
        banco.close();
        super.onDestroy();
    }

    public void confirmar(View v ) {
        EditText ed = (EditText) findViewById(R.id.ed_setor);
        String setor = ed.getText().toString().trim();
        if ( ! setor.isEmpty() ) {
            ContentValues cv = new ContentValues();
            cv.put("nome", setor);
            long id = banco.insert("setor", null, cv);
            if (id > 0) {
                atualizarLista();
            } else {
                Toast.makeText(this, "Falha na inserção", Toast.LENGTH_SHORT).show();
            }
        }
        ed.setText("");
    }

    private void atualizarLista() {
        cursor.close();
        cursor = banco.query("setor", new String[] {"_id", "nome"},
                null, null, null, null, "nome");
        adapter.changeCursor(cursor);
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int pos, long id) {
        banco.delete("setor", "_id = ?", new String[] { String.valueOf(id)} );
        atualizarLista();
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long id) {
        cursor.moveToPosition(i);
        Intent it = new Intent(this, Produtos.class);
        it.putExtra("id_setor", id);
        it.putExtra("nome_setor", cursor.getString(1));
        startActivity(it);
    }
}